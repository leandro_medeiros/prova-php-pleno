# Prova - PHP Pleno #

http://bitbucket.org/leandro_medeiros/prova-php-pleno

### Sobre ###
Aplicação WEB com a resolução do teste para a vaga de PHP Pleno, cujo PDF original pode ser encontrado neste repositório em </documentation/Prova PHP Pleno.pdf>.

### Licença ###
Este software é gratuito e distribuído sob a ***GPLv3***. Veja <licence/gpl.txt> OU [GNU Org](http://www.gnu.org/licenses/) para maiores informações.

### Add-ons ###
Junto à esta *prova* são distribuídas cópias de softwares complementares:

- O back-end foi construído sobre o [MonsterfyMVC](http://bitbucket.org/leandro_medeiros/monsterfymvc), um micro-framework [que desenvolvi](http://about.me/leandro.medeiros) de 2012 à 2015, quando comecei à utilizar o PhalconPHP e descontinuei o projeto.
- O front-end é construído sobre o [framework Bootstrap](http://getbootstrap.com).
- As grades são reprocessadas pelo [plugin DataTables](http://datatables.net).

### Pré-requisitos ###
Ambiente de desenvolvimento:

1. [MySQL Server 5.6+](http://dev.mysql.com/downloads/). (Pode-se rodar o MySQL em qualquer outro computador, desde que o usuário tenha permissões remotas);
2. [Apache Server 2.0+](http://apache.org) OU [NGINX 1.7.10+](http://nginx.org>);
3. [Pré-processador PHP 5.3+](http://php.net/downloads.php);

Recomendação: Utilizar uma SolutionStack como o MAMP (Mac OS X), WAMP (MS Windows) ou LAMP (OS baseado em Linux) para facilitar a montagem do ambiente.

### Execução ###

1. Clonar este repositório para o diretório ROOT configurado no seu servidor HTTP;
2. Executar o script </documentation/mysql/scripts/01-create.sql> (renomeie o banco de dados à vontade);
3. Alterar as constantes da classe Config <commons/Config.php> com os dados de acesso ao seu servidor MySQL (host, porta, nome do banco, usuário e senha);
4. Pronto! Ao acessar seu [local host](http://localhost/prova-php-pleno) você verá a lista de questões!

## Contato ##
***Leandro Medeiros***

* [LinkedIn](https://br.linkedin.com/in/medeirosleandro)

* [GitHub](https://github.com/leandrommedeiros)

* [Twitter](https://twitter.com/Medeiros_1991)

* [About.Me](https://about.me/leandro.medeiros)
