<header class="jumbotron subhead" id="question02">
    <div class="hero-unit">
        <h2>Questão 02</h2>
        <p class="lead">Refatore o código abaixo, fazendo as alterações que julgar necessário:</p>
        <blockquote>
            <pre>
                <code>
    1. &lt;?php
    2.
    3. if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
    4.     header("Location: http://www.google.com");
    5.     exit();
    6. } elseif (isset($_COOKIE['Loggedin']) && $_COOKIE['Loggedin'] == true) {
    7.     header("Location: http://www.google.com");
    8.     exit();
    9. }
                </code>
            </pre>
        </blockquote>

        <h2>Resposta</h2>
        <p class="lead">A função <strong>empty()</strong> valida se o índice existe no array e se o valor é diferente de <i>null</i>, <i>false</i>, <i>0</i> e <i>''</i> (string vazia).</p>
        <p class="lead">A função <strong>header()</strong> com o parâmetro <i>"Location"</i> interrompe a execução do script, logo o <strong>exit()</strong> é desnecessário.</p>
        <blockquote>
            <pre>
                <code>
    1. &lt;?php
    2.
    3. if (!empty($_SESSION['loggedin']) || !empty($_COOKIE['Loggedin'])) {
    4.     header("Location: http://www.google.com");
    5. }
                </code>
            </pre>
        </blockquote>
    </div>
</header>
