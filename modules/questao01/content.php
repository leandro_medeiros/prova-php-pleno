<header class="jumbotron subhead" id="question01">
    <div class="hero-unit">
        <h2>Questão 01</h2>
        <p class="lead">
            Escreva um programa que imprima números de 1 a 100. Mas, para múltiplos de 3 imprima “Fizz” em vez do número e para múltiplos de 5 imprima “Buzz”. Para números múltiplos de ambos (3 e 5), imprima “FizzBuzz”
        </p>

        <h2>Resposta</h2>
        <p>
            <button data-toggle="modal" class="btn btn-default" data-target="#window-code">
                <span class="glyphicon glyphicon-cog"></span>&nbsp;&nbsp;Visualizar Código
            </button>
            <button data-toggle="modal" class="btn btn-success" data-target="#window-run">
                <span class="glyphicon glyphicon-play"></span>&nbsp;&nbsp;Executar
            </button>
        </p>
    </div>
</header>

<div id="window-code" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" align="center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <span class="glyphicon glyphicon-file" aria-hidden="true"></span>
                    <span id="window-code-title">Questão 01 - Resposta</span>
                </h4>
            </div>

            <div class="modal-body" id="window-code-body">
                <blockquote>
                    <pre>
                        <code>
    01. &lt;?php
    02.
    03. $values = array();
    04.
    05. for ($idx = 1; $idx <= 100; $idx++) {
    06.     $result = '';
    07.     if ($idx % 3 == 0) {
    08.         $result .= 'Fizz';
    09.     }
    10.     if ($idx % 5 == 0) {
    11.         $result .= 'Buzz';
    12.     }
    13.
    14.     $values[$idx] = empty($result) ? $idx : $result;
    15. }
    16.
    17. echo implode(&quot;&lt;br />&quot;, $values);
    18.
    19. ?>
                        </code>
                    </pre>
                </blockquote>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">
                    Fechar <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="window-run" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" align="center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <span class="glyphicon glyphicon-file" aria-hidden="true"></span>
                    <span id="window-run-title">Questão 01 - Resposta</span>
                </h4>
            </div>

            <div class="modal-body" id="window-run-body">
                <pre>
<?php

$values = array();

for ($idx = 1; $idx <= 100; $idx++) {
    $result = '';
    if ($idx % 3 == 0) {
        $result .= 'Fizz';
    }
    if ($idx % 5 == 0) {
        $result .= 'Buzz';
    }

    $values[$idx] = empty($result) ? $idx : $result;
}

echo implode('<br />', $values);

?>
                </pre>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">
                    Fechar <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
