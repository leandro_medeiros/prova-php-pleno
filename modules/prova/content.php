<center>
    <div class="row">
        <?php foreach ($this->data as $Dto): ?>
            <?php if ($this->module != $Dto->name): ?>
                <div class="col-sm-6 col-md-4">
            	    <div class="caption">
            	    	<h3>
        	    	    	<a href="../<?php echo $Dto->name; ?>" class="thumbnail">
                    	    	<img alt="imagem embarcada" src="<?php echo PATH_IMAGE . $Dto->icon; ?>" style="height:200px;" />
                            	<?php echo $Dto->title; ?>
                    		</a>
                    	</h3>
                    </div>
        		</div>
            <?php endif;?>
        <?php endforeach;?>
    </div>
</center>
