<!-- Executa o "Require" do JS correspondente -->
<input type="hidden" class="page-id" data-page="tasks" />

<header class="jumbotron subhead" id="question02">
    <div class="hero-unit">
        <h2>Questão 04 - API</h2>
        <p class="lead">
            Desenvolva uma API Rest para um sistema gerenciador de tarefas (inclusão/alteração/exclusão). As tarefas consistem em título e descrição, ordenadas por prioridade. Desenvolver utilizando:
        </p>
        <ul>
            <li>Linguagem PHP (ou framework CakePHP);</li>
            <li>Banco de dados MySQL;</li>
        </ul>
        <p class="lead">Diferenciais:</p>
        <ul>
            <li>Criação de interface para visualização da lista de tarefas;</li>
            <li>Interface com drag and drop;</li>
            <li>Interface responsiva (desktop e mobile);</li>
        </ul>

        <h2>Resposta</h2>

        <p>
            <a href="../../ws/json_server.php" target="blank">
                <span class="glyphicon glyphicon-share-alt"></span>&nbsp;API REST (Endpoint)
            </a>
        </p>
        <p>
            <a href="../../ws/server.php?wsdl" target="blank">
                <span class="glyphicon glyphicon-globe"></span>&nbsp;API SOAP (WSDL)
            </a>
        </p>
        <p>
            <a data-toggle="modal" data-target="#window-tasks" href="#">
                <span class="glyphicon glyphicon-list"></span>&nbsp;Lista de Tarefas
            </a>
        </p>
    </div>
</header>

<div id="window-tasks" class="modal fade bs-example-modal-lg">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" align="center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <span class="glyphicon glyphicon-list" aria-hidden="true"></span>
                    <span id="window-tasks-title">Tarefas</span>
                </h4>
            </div>

            <div class="modal-body" id="window-tasks-body">

                <!-- Filtros -->
                <form id="tasks-filters" method="post" action="./" >
                    <input type="hidden" name="action" value= "listTasks" />
                </form>

                <!-- Lista de Tarefas -->
                <table id="table-tasks" class="datatables" form-filters="tasks-filters">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Tarefa</th>
                            <th>Descrição</th>
                            <th>Prioridade</th>
                            <th>Atribuído à</th>
                        </tr>
                    </thead>
                    <!--tbody>
                        <?php #foreach ($this->data as $Dto): ?>
                            <tr>
                                <td title="Identificação"><?php #echo $Dto->id; ?></td>
                                <td title="Tarefa"><?php #echo $Dto->title; ?></td>
                                <td title="Breve resumo"><?php #echo $Dto->description; ?></td>
                                <td title="Prioridade"><?php #echo $Dto->priority; ?></td>
                                <td title="Usuário que inseriu a tarefa"><?php #echo $Dto->user; ?></td>
                            </tr>
                        <?php #endforeach;?>
                    </tbody-->
                </table>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary trigger-list-refresh">
                    Atualizar <span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
