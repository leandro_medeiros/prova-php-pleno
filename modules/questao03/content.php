<header class="jumbotron subhead" id="question03">
    <div class="hero-unit">
        <h2>Questão 03</h2>
        <p class="lead">Refatore o código abaixo, fazendo as alterações que julgar necessário:</p>
        <blockquote>
            <pre>
                <code>
    1. &lt;?
    2.
    3. class MyUserClass
    4. {
    5.     public function getUserList()
    6.     {
    7.         $dbconn = new DatabaseConnection('localhost','user','password');
    8.         $results = $dbconn->query('select name from user');
    9.
    10.        sort($results);
    11.
    12.        return $results;
    13.     }
    14. }
                </code>
            </pre>
        </blockquote>

        <h2>Resposta</h2>
        <p class="lead">
            Os parâmetros de conexão com o banco de dados não deve estar no código, principalmente em uma classe, o ideal é utilizar constantes (arquivo 02). Para facilitar a manutenção e evitar que equipes de infra / suporte alterem fontes, lê-mos os parâmetros de um INI na raiz da aplicação (arquivo 01). A conexão deve ser estabelecida ao instânciar a classe onde será usada para diminuir a carga de memória do servidor ou recebida no construtor para aplicar injeção de dependência (arquivo 03).
        </p>
        <br />
        <p>
            <a data-toggle="modal" data-target="#window-file01" href="#">
                <span class="glyphicon glyphicon-file"></span>&nbsp;&nbsp;01 - Visualizar "/config.ini"
            </a>
        </p>
        <p>
            <a data-toggle="modal" data-target="#window-file02" href="#">
                <span class="glyphicon glyphicon-file"></span>&nbsp;&nbsp;02 - Visualizar "/app/common/config.php"
            </a>
        </p>
        <p>
            <a data-toggle="modal" data-target="#window-file03" href="#">
                <span class="glyphicon glyphicon-file"></span>&nbsp;&nbsp;03 - Visualizar "/app/public/MyUserClass.php"
            </a>
        </p>
    </div>
</header>

<div id="window-file01" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" align="center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <span class="glyphicon glyphicon-file" aria-hidden="true"></span>
                    <span id="window-file01-title">/config.ini</span>
                </h4>
            </div>

            <div class="modal-body" id="window-file01-body">
                <blockquote>
                    <pre>
                        <code>
    1. [DATABASE]
    2. host=localhost
    3. user=user
    4. passwd=password
                        </code>
                    </pre>
                </blockquote>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">
                    Fechar <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="window-file02" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" align="center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <span class="glyphicon glyphicon-file" aria-hidden="true"></span>
                    <span id="window-file02-title">/app/common/config.php</span>
                </h4>
            </div>

            <div class="modal-body" id="window-file02-body">
                <blockquote>
                    <pre>
                        <code>
    1. &lt;?php
    2.     # Procedural
    3.     $inifile = parse_ini_file('../../config.ini', true);
    4.
    5.     define('DB_HOST',     $inifile['DATABASE']['host']);
    6.     define('DB_USER',     $inifile['DATABASE']['user']);
    7.     define('DB_PASSWORD', $inifile['DATABASE']['passwd']);
    8. ?>
                        </code>
                    </pre>
                </blockquote>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">
                    Fechar <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="window-file03" class="modal fade bs-example-modal-lg">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" align="center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <span class="glyphicon glyphicon-file" aria-hidden="true"></span>
                    <span id="window-file03-title">/app/public/MyUserClass.php</span>
                </h4>
            </div>

            <div class="modal-body" id="window-file03-body">
                <blockquote>
                    <pre>
                        <code>
    01. &lt;?php
    02. class MyUserClass
    03. {
    04.     public $dbconn = null;
    05.
    06.     /**
    07.      * Construtor
    08.      *
    09.      * Estabelece uma conexão com a base de dados
    10.      *
    11.      * @author Leandro Medeiros &lt;leandro.medeiros@live.com>
    12.      * @since  2016-11-27
    13.      */
    14.     public function __construct()
    15.     {
    16.         $this->dbconn = new DatabaseConnection(DB_HOST, DB_USER, DB_PASSWORD);
    17.     }
    18.
    19.     /**
20.      * Retorna a lista de usuários cadastrados em ordem alfabética (nome).
    21.      *
    22.      * @author Leandro Medeiros &lt;leandro.medeiros@live.com>
    23.      * @since  2016-11-27
    24.      *
    25.      * @return array
    26.      */
    27.     public function getUserList()
    28.     {
    29.         return sort($this->dbconn->query(
    30.             'SELECT u.name
    31.                FROM user u
    32.            ORDER BY name ASC'
    33.         ));
    34.     }
    35. }
    36.
    37. ?>
                        </code>
                    </pre>
                </blockquote>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">
                    Fechar <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
