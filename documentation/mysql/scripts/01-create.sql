-- MySQL Script generated by MySQL Workbench
-- Mon Aug  3 00:31:41 2015
-- Model: Relationships    Version: 2.1
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema php_pleno
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `php_pleno` ;

-- -----------------------------------------------------
-- Schema php_pleno
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `php_pleno` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `php_pleno` ;

-- -----------------------------------------------------
-- Table `php_pleno`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `php_pleno`.`user` ;

CREATE TABLE IF NOT EXISTS `php_pleno`.`user` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `active` TINYINT(1) NOT NULL DEFAULT 1,
  `email` VARCHAR(100) NOT NULL,
  `password` CHAR(32) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE UNIQUE INDEX `email_UNIQUE` ON `php_pleno`.`user` (`email` ASC);


-- -----------------------------------------------------
-- Table `php_pleno`.`module`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `php_pleno`.`module` ;

CREATE TABLE IF NOT EXISTS `php_pleno`.`module` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `active` TINYINT(1) NOT NULL DEFAULT 1,
  `title` VARCHAR(100) NOT NULL,
  `name` VARCHAR(45) NOT NULL DEFAULT 'module',
  `menu_order` INT UNSIGNED NOT NULL,
  `icon` VARCHAR(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE UNIQUE INDEX `name_UNIQUE` ON `php_pleno`.`module` (`name` ASC);


-- -----------------------------------------------------
-- Table `php_pleno`.`user_module`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `php_pleno`.`user_module` ;

CREATE TABLE IF NOT EXISTS `php_pleno`.`user_module` (
  `user_id` INT UNSIGNED NOT NULL,
  `module_id` INT UNSIGNED NOT NULL,
  `favorite` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`user_id`, `module_id`),
  CONSTRAINT `fk_user_module_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `php_pleno`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_module_module`
    FOREIGN KEY (`module_id`)
    REFERENCES `php_pleno`.`module` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_user_module_module_idx` ON `php_pleno`.`user_module` (`module_id` ASC);

CREATE INDEX `fk_user_module_user_idx` ON `php_pleno`.`user_module` (`user_id` ASC);


-- -----------------------------------------------------
-- Table `php_pleno`.`tab`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `php_pleno`.`tab` ;

CREATE TABLE IF NOT EXISTS `php_pleno`.`tab` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `active` TINYINT(1) NOT NULL DEFAULT 1,
  `module_id` INT UNSIGNED NOT NULL,
  `title` VARCHAR(45) NOT NULL,
  `action` VARCHAR(45) NOT NULL,
  `menu_order` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_tab_module`
    FOREIGN KEY (`module_id`)
    REFERENCES `php_pleno`.`module` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_tab_module_idx` ON `php_pleno`.`tab` (`module_id` ASC);

-- -----------------------------------------------------
-- Table `php_pleno`.`priority`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `php_pleno`.`priority` ;

CREATE TABLE IF NOT EXISTS `php_pleno`.`priority` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `active` TINYINT(1) NOT NULL DEFAULT 1,
  `description` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`)
)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `php_pleno`.`task`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `php_pleno`.`task` ;

CREATE TABLE IF NOT EXISTS `php_pleno`.`task` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `active` TINYINT(1) NOT NULL DEFAULT 1,
  `user_id` INT UNSIGNED NOT NULL,
  `title` VARCHAR(45) NOT NULL,
  `description` VARCHAR(100) NOT NULL,
  `priority_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_task_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `php_pleno`.`user` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_task_priority`
    FOREIGN KEY (`priority_id`)
    REFERENCES `php_pleno`.`priority` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_task_priority_idx` ON `php_pleno`.`task` (`priority_id` ASC);

USE `php_pleno` ;

-- -----------------------------------------------------
-- Placeholder table for view `php_pleno`.`vw_modules`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `php_pleno`.`vw_modules` (`userId` INT, `moduleId` INT, `favorite` INT, `moduleOrder` INT, `moduleActive` INT, `moduleTitle` INT, `moduleName` INT, `moduleIcon` INT, `tabId` INT, `tabOrder` INT, `tabTitle` INT, `tabAction` INT);

-- -----------------------------------------------------
-- View `php_pleno`.`vw_modules`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `php_pleno`.`vw_modules` ;
DROP TABLE IF EXISTS `php_pleno`.`vw_modules`;
USE `php_pleno`;
CREATE  OR REPLACE VIEW `vw_modules` AS
  SELECT `user_module`.`user_id` AS `userId`,
       `module`.`id` AS `moduleId`,
           `user_module`.`favorite`,
           `module`.`menu_order` AS `moduleOrder`,
           `module`.`active` AS `moduleActive`,
           `module`.`title` AS `moduleTitle`,
           `module`.`name` AS `moduleName`,
           `module`.`icon` AS `moduleIcon`,
           `tab`.`id` AS `tabId`,
           `tab`.`menu_order` AS `tabOrder`,
           `tab`.`title` AS `tabTitle`,
           `tab`.`action` AS `tabAction`
    FROM user_module
      JOIN module ON (`user_module`.`module_id` = `module`.`id`)
      JOIN tab ON (`module`.`id` = `tab`.`module_id`)
  ORDER BY `user_module`.`user_id`,
           `module`.`menu_order`,
           `tab`.`menu_order`;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `php_pleno`.`user`
-- -----------------------------------------------------
START TRANSACTION;
USE `php_pleno`;
INSERT INTO `php_pleno`.`user` (`id`, `active`, `email`, `password`, `name`) VALUES (1, 1, 'admin@php_pleno.com', 'a946ae1df51a10613a6e5624d1daa82c', 'Administrador');
INSERT INTO `php_pleno`.`user` (`id`, `active`, `email`, `password`, `name`) VALUES (2, 1, 'visitante@php_pleno.com', 'cab5a031f5506b862b7487f987edbd68', 'Visitante');

COMMIT;


-- -----------------------------------------------------
-- Data for table `php_pleno`.`module`
-- -----------------------------------------------------
START TRANSACTION;
USE `php_pleno`;
INSERT INTO `php_pleno`.`module` (`id`, `active`, `title`, `name`, `menu_order`, `icon`) VALUES (1, 1, 'Prova PHP Pleno', 'prova', 1, '');
INSERT INTO `php_pleno`.`module` (`id`, `active`, `title`, `name`, `menu_order`, `icon`) VALUES (2, 1, 'Questão 01', 'questao01', 1, 'questao01.jpg');
INSERT INTO `php_pleno`.`module` (`id`, `active`, `title`, `name`, `menu_order`, `icon`) VALUES (3, 1, 'Questão 02', 'questao02', 1, 'questao02.jpg');
INSERT INTO `php_pleno`.`module` (`id`, `active`, `title`, `name`, `menu_order`, `icon`) VALUES (4, 1, 'Questão 03', 'questao03', 1, 'questao03.jpg');
INSERT INTO `php_pleno`.`module` (`id`, `active`, `title`, `name`, `menu_order`, `icon`) VALUES (5, 1, 'Questão 04 - API', 'api', 1, 'api.png');

COMMIT;


-- -----------------------------------------------------
-- Data for table `php_pleno`.`user_module`
-- -----------------------------------------------------
START TRANSACTION;
USE `php_pleno`;
INSERT INTO `php_pleno`.`user_module` (`user_id`, `module_id`, `favorite`) VALUES (1, 1, 0);
INSERT INTO `php_pleno`.`user_module` (`user_id`, `module_id`, `favorite`) VALUES (2, 1, 0);
INSERT INTO `php_pleno`.`user_module` (`user_id`, `module_id`, `favorite`) VALUES (1, 2, 0);
INSERT INTO `php_pleno`.`user_module` (`user_id`, `module_id`, `favorite`) VALUES (2, 2, 0);
INSERT INTO `php_pleno`.`user_module` (`user_id`, `module_id`, `favorite`) VALUES (1, 3, 0);
INSERT INTO `php_pleno`.`user_module` (`user_id`, `module_id`, `favorite`) VALUES (2, 3, 0);
INSERT INTO `php_pleno`.`user_module` (`user_id`, `module_id`, `favorite`) VALUES (1, 4, 0);
INSERT INTO `php_pleno`.`user_module` (`user_id`, `module_id`, `favorite`) VALUES (2, 4, 0);
INSERT INTO `php_pleno`.`user_module` (`user_id`, `module_id`, `favorite`) VALUES (1, 5, 0);
INSERT INTO `php_pleno`.`user_module` (`user_id`, `module_id`, `favorite`) VALUES (2, 5, 0);

COMMIT;


-- -----------------------------------------------------
-- Data for table `php_pleno`.`tab`
-- -----------------------------------------------------
-- START TRANSACTION;
-- USE `php_pleno`;
-- INSERT INTO `php_pleno`.`tab` (`id`, `active`, `module_id`, `title`, `action`, `menu_order`) VALUES (1, 1, 1, 'Questoes', 'listQuestions', 1);
-- INSERT INTO `php_pleno`.`tab` (`id`, `active`, `module_id`, `title`, `action`, `menu_order`) VALUES (2, 2, 1, 'Enunciado', 'showQuestion', 1);
-- INSERT INTO `php_pleno`.`tab` (`id`, `active`, `module_id`, `title`, `action`, `menu_order`) VALUES (3, 2, 1, 'Resposta', 'showResolution', 1);
-- INSERT INTO `php_pleno`.`tab` (`id`, `active`, `module_id`, `title`, `action`, `menu_order`) VALUES (4, 3, 1, 'Enunciado', 'showQuestion', 1);
-- INSERT INTO `php_pleno`.`tab` (`id`, `active`, `module_id`, `title`, `action`, `menu_order`) VALUES (5, 3, 1, 'Resposta', 'showResolution', 1);
-- INSERT INTO `php_pleno`.`tab` (`id`, `active`, `module_id`, `title`, `action`, `menu_order`) VALUES (6, 4, 1, 'Enunciado', 'showQuestion', 1);
-- INSERT INTO `php_pleno`.`tab` (`id`, `active`, `module_id`, `title`, `action`, `menu_order`) VALUES (7, 4, 1, 'Resposta', 'showResolution', 1);
-- INSERT INTO `php_pleno`.`tab` (`id`, `active`, `module_id`, `title`, `action`, `menu_order`) VALUES (8, 5, 1, 'API', 'show', 1);

-- COMMIT;

-- -----------------------------------------------------
-- Data for table `php_pleno`.`tab`
-- -----------------------------------------------------
START TRANSACTION;
USE `php_pleno`;
INSERT INTO `php_pleno`.`priority` (`id`, `description`) VALUES (1, 'PARADA DE OPERAÇÃO');
INSERT INTO `php_pleno`.`priority` (`id`, `description`) VALUES (2, 'ALTA');
INSERT INTO `php_pleno`.`priority` (`id`, `description`) VALUES (3, 'MÉDIA');
INSERT INTO `php_pleno`.`priority` (`id`, `description`) VALUES (4, 'BAIXA');
INSERT INTO `php_pleno`.`priority` (`id`, `description`) VALUES (5, 'MELHORIA');

COMMIT;
