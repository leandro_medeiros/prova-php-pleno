<?php

require_once __DIR__ . '/Endpoint.php';

if (!isset($_GET['url'])) {
    include_once 'restdoc.html';
} else {
    $urlParts = explode('/', $_GET['url']);

    if (count($urlParts) >= 2) {
        try {
            $class = $urlParts[0];
            $action = $urlParts[1];
            $payload = file_get_contents('php://input');
            $payload = iconv('UTF-8', 'UTF-8//IGNORE', utf8_encode($payload));

            $jsonRequest = @json_decode($payload);

            if (is_null($jsonRequest)) {
                throw new InvalidArgumentException('Requisição Inválida');
            }

            $instance = new Endpoint;
            $refl = new ReflectionClass($instance);

            $method = $refl->getMethod($action);
            $methodDoc = $method->getDocComment();

            $args = array();
            $params = $method->getParameters();

            foreach ($params as $param) {
                $name = $param->getName();
                $value = null;

                if (isset($jsonRequest->$name)) {
                    $value = $jsonRequest->$name;
                } else if (!$param->isOptional()) {
                    throw new InvalidArgumentException($name . ' é obrigatório');
                }

                $paramClass = $param->getClass();

                if (!empty($paramClass)) {
                    $value = toTypedValue($paramClass->newInstance(), $value);
                } else {
                    preg_match('#@param\s*(\w+(\\\\\w+)*)(\[\])?\s*\$' . $param->getName() . '#mi', trim($methodDoc), $reg);

                    if (!empty($reg[0]) && class_exists($reg[1])) {
                        $className = $reg[1];
                        $isArray = !empty($reg[3]);

                        if ($isArray && is_array($value)) {
                            $newValue = array();

                            foreach ($value as $item) {
                                $item = toTypedValue(new $className(), $item);
                                $newValue[] = $item;
                            }

                            $value = $newValue;
                        } else {
                            $value = toTypedValue(new $className(), $value);
                        }
                    }
                }

                $args[] = $value;
            }

            $result = call_user_func_array(array($instance, $action), $args);

            echo json_encode($result);

        } catch (Exception $ex) {
            echo json_encode(array(
                'type' => 'exception',
                'code' => $ex->getCode(),
                'message' => $ex->getMessage(),
            ));
        }
    }
}

/**
 * Transforma o valor enviado em formato de objeto normal para tipado.
 *
 * @param object $instance
 * @param object $sourceValue
 * @return object
 */
function toTypedValue($instance, $sourceValue)
{
    static $ignoreClasses = array('datetime');

    foreach (get_object_vars($instance) as $key => $val) {
        $refl = new ReflectionClass($instance);
        $fieldDoc = $refl->getProperty($key)->getDocComment();

        if (isset($sourceValue->$key)) {
            if (preg_match('#@var\s*(\w+(\\\\\w+)*)+#', trim($fieldDoc), $reg) && class_exists($reg[1]) && !in_array(strtolower($reg[1]), $ignoreClasses)) {
                $instance->$key = toTypedValue(new $reg[1](), $sourceValue->$key);
            } else {
                $instance->$key = $sourceValue->$key;
            }
        }
    }

    return $instance;
}
