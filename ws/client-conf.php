<?php

header('Content-Type: text/plain');

include_once __DIR__ . '/../libraries/SplClassLoader.php';

$loader = new SplClassLoader('beans', __DIR__);
$loader->register();

$result = null;

$dir = 'beans';
$map = array();

foreach (new RegexIterator(new DirectoryIterator(__DIR__ . '/' . $dir), '@\.php$@i') as $file) {
    $from = str_replace('.php', '', $file->getBasename());
    $to = $dir . '\\' . str_replace('.php', '', $file->getBasename());
    $map[$from] = $to;
}

$url = 'http://127.0.0.1/php_pleno/php/ws/server.php?wsdl';

$client = new SoapClient($url, array(
    'cache_wsdl' => WSDL_CACHE_NONE,
    'classmap' => $map,
    "trace" => 1,
));

//////////////// END - DEFINICAO CLIENT //////////////////////////////
