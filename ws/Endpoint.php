<?php

require_once '../application/bootstrap.php';

/**
 * Endpoint
 *
 * @package ws
 * @author  Leandro Medeiros
 * @since   2016-11-27
 * @link    http://bitbucket.org/leandro_medeiros/monsterfymvc
 */
class Endpoint
{
    /**
     * Inclui uma tarefa.
     *
     * @author Leandro Medeiros <leandro.medeiros@live.com>
     * @since  2016-11-27
     *
     * @param  TaskDTO      $Dto      Dados da tarefa
     * @return boolean                Sucesso na inclusão
     */
    public function insertTask(TaskDTO $Dto)
    {
        $Task = new Task($Dto);
        return $Task->insert();
    }

    /**
     * Atualiza uma tarefa.
     *
     * @author Leandro Medeiros <leandro.medeiros@live.com>
     * @since  2016-11-27
     *
     * @param  TaskDTO      $Dto      Dados da tarefa
     * @return boolean                Sucesso na inclusão
     */
    public function updateTask(TaskDTO $Dto)
    {
        $Task = new Task($Dto);
        return $Task->update();
    }

    /**
     * Remove (desativa) uma tarefa.
     *
     * @author Leandro Medeiros <leandro.medeiros@live.com>
     * @since  2016-11-27
     *
     * @param  TaskDTO      $Dto      Dados da tarefa
     * @return boolean                Sucesso ao desativar
     */
    public function deleteTask($id)
    {
        $Dto = new TaskDTO;
        $Dto->id = $id;
        $Dto->active = false;

        $Task = new Task($Dto);

        return $Task->update();
    }
}
