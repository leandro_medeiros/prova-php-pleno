<?php

/**
 * Exceção de WebService
 *
 * @package ws
 * @author  Leandro Medeiros
 * @since   2016-11-27
 * @link    http://bitbucket.org/leandro_medeiros/monsterfymvc
 */
class WebServiceException extends Exception
{
    const WS_ERR_USUARIO_SENHA_INVALIDA = 101;

    /**
     * Mensagens de erro
     * @var array
     */
    private static $errors = array(
        101 => 'Usuário/senha inválida',
    );

    /**
     * Cria uma excecao com o codigo informado
     * @param int $code
     * @param string|array $message
     * @return WebServiceException
     */
    public static function createException($code, $message = null)
    {
        if (is_array($message) && !empty($message)) {
            $message = implode(PHP_EOL, $message);
        }

        if (empty($message)) {
            $message = isset(self::$errors[$code]) ? self::$errors[$code] : $message;
        }

        return new self($message, $code);
    }
}
