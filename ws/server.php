<?php

try {
    ini_set('soap.wsdl_cache_enabled', 0);

    require_once __DIR__ . '/Endpoint.php';
    require_once __DIR__ . '/libraries/WSDLDocument.php';

    if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
        $protocol = 'https://';
    } else {
        $protocol = 'http://';
    }

    $url = "{$protocol}{$_SERVER['HTTP_HOST']}{$_SERVER['SCRIPT_NAME']}";

    if (isset($_GET['wsdl'])) {
        header('Content-Type: text/xml');
        $wsdl = new WSDLDocument('Endpoint', null, 'urn:EndpointWsdl');
        echo $wsdl->saveXML();
        exit;

    } else {
        $server = new SoapServer($url . '?wsdl', array('classmap' => $map));
        $server->setClass('Endpoint');
        $server->handle();
    }
} catch (Exception $ex) {
    if ($ex instanceof WebServiceException) {
        throw $ex;
    } else {
        Lib::log($ex);
    }
}
