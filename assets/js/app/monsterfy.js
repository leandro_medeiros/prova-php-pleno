define(['jquery'], function($) {

    function initialize() {
        $('.page-id').each( function (idx, obj) {
            getPage($(obj).attr('data-page'));
        } );

        $(".modal-wide").on("show.bs.modal", function() {
            var height = $(window).height() - 100;
            $(this).find(".modal-body").css("max-height", height);
        });

        $('.call-action').click(function() {
            var action = $(this).attr('data-action');
            var module = $(this).attr('data-module');
            var form   = $(this).attr('data-form');
            callAction(action, module, form);
        });

        $('#btn-logoff').click(function() {
            bootbox.confirm('Deseja mesmo sair da aplicação?', function(result) {
                if (result === true) post( {action: 'logout'} );
            });
        });
    }

    function getPage(pageID) {
        require([pageID], function(Module) {
            if (typeof Module != 'undefined') {
                Module.initialize();
            }
        });
    }

    function callAction(actionKey, module, form) {

        if (typeof form == 'undefined' || form == '') {
            document.getElementById('btn-hidden').value    = (typeof actionKey == 'undefined') ? '' : actionKey;
            document.getElementById('module-hidden').value = (typeof module == 'undefined')   ? '' : module;
            document.getElementById('frm-hidden').submit();
        }
        else {
            document.getElementById('action').value = (typeof actionKey == 'undefined') ? '' : actionKey;
            document.getElementById('module').value = (typeof module == 'undefined') ? '' : module;
            document.getElementById(form).submit();
        }
    }

    function submitForm(idForm, idCallback, module) {
        $(document.body).on('click', '#'+idCallback, function() {
            document.getElementById('module-hidden').value = module;
            $('#'+idForm).submit();
        });
    }

    function post(params, path, method) {
        path   = path   || './';
        method = method || 'POST';

        var form = document.createElement('form');
        form.setAttribute('method', method);
        form.setAttribute('action', path);

        for (var key in params) {
            if (params.hasOwnProperty(key)) {
                var hiddenField = document.createElement('input');
                hiddenField.setAttribute('type',  'hidden');
                hiddenField.setAttribute('name',  key);
                hiddenField.setAttribute('value', params[key]);

                form.appendChild(hiddenField);
             }
        }

        document.body.appendChild(form);
        form.submit();
    }

    function getScreenSize() {
        return $(window).width();
    }

    /**
     * Submeter formulario com post para chegada na controller
     */
    function submitForm(formId, action) {
        document.getElementById('method').value = action;
        document.getElementById(formId).submit();
    }

    /**
     * Usa o rodapé da página para exibir algum conteúdo de Log
     *
     * @author Leandro Medeiros <leandro.medeiros@live.com>
     * @since  2016-11-27
     *
     * @param  {mixed}    data   Conteúdo à ser exibido
     * @return {boolean}
     */
    function footerLog(data) {
        var div = $('#footer-js-log');

        if (div) {
            $('#footer-js-log-content').html('<pre>' + JSON.stringify(data) + '</pre>');

            div.show();

            return true;
        }

        return false;
    }

    function appendAlert(parentSelector, message, replace) {
        if (typeof(replace) === 'undefined') replace = false;

        message = '<div id="" style="padding:10px 35px 10px 10px" class="alert text-left alert-danger alert-dismissible" role="alert">'
                + message
                + '<button type="button" class="close" data-dismiss="alert" aria-label="Fechar"><span aria-hidden="true">×</span></button></div>';

        if (replace) $(parentSelector).html(message);
        else         $(parentSelector).append(message);
    }

    function getAjaxForm(formSelector) {
        var form   = $('#' + $(table).attr('form-filters'));
        var values = {};

        $.each($(form).serializeArray(), function(i, field) {
            values[field.name] = field.value;
        });

        return {
            type     : $(form).attr('method'),
            url      : $(form).attr('action'),
            dataType : 'json',
            data     : values
        }
    }

    /* Métodos públicos que serão utilizados por outras bibliotecas externas */
    return {
        initialize    : initialize,
        callAction    : callAction,
        submitForm    : submitForm,
        getScreenSize : getScreenSize,
        submitForm    : submitForm,
        footerLog     : footerLog,
        appendAlert   : appendAlert,
        getAjaxForm   : getAjaxForm,
        post          : post
    };
});
