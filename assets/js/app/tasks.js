define(['jquery', 'datatables', 'monsterfy', 'bootbox'], function($, Datatables, App, bootbox) {

    /**
     * Inicializador dos eventos da API (Tarefas)
     *
     * @author Leandro Medeiros <leandro.medeiros@live.com>
     * @since  2016-11-27
     *
     * @return {null}
     */
    function initialize() {
        Datatables.initialize('#table-tasks', [
            null,
            null,
            null,
            null,
            {"sType": "html", "sClass": "cell-actions", "bSortable": false}
        ]);

        /** Ao exibir o modal **/
        $('#window-tasks').on('shown.bs.modal', function (event) {
            refresh();
        });

        /** Classe "Buscar" **/
        $('body').on('click', '.trigger-list-refresh', function(event) {
            event.preventDefault();
            refresh();
        });
    }

    /**
     * Atualiza a grade.
     *
     * @author Leandro Medeiros <leandro.medeiros@live.com>
     * @since  2016-11-27
     *
     * @return {null}
     */
    function refresh() {
        Datatables.refresh('#table-tasks');
    }


    return {
        initialize : initialize
    };
});
