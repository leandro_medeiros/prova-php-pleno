requirejs.config({
    urlArgs: "bust=v130."+ Math.random(),
    baseUrl: "../../assets/js/app/",
    waitSeconds: 30,
    shim: {
        'monsterfy': {
            deps    : ['jquery', 'bootbox'],
            exports : 'monsterfy'
        },

        'bootstrap': {
            deps    : ['jquery'],
            exports : 'bootstrap'
        },

        'bootbox': {
            deps    : ['jquery'],
            exports : 'bootbox'
        },

        'bootstrapDatepicker': {
            deps    : ['jquery'],
            exports : 'bootstrapDatepicker'
        },

        'bootstrapSelect': {
            deps    : ['jquery'],
            exports : 'bootstrapSelect'
        },

        'dataTables': {
            deps    : ['jquery'],
            exports : 'dataTables'
        }
    },
    paths: {
        jquery                : "../vendor/jquery.min",
        bootstrap             : "../vendor/bootstrap.min",
        bootbox               : "../vendor/bootbox.min",
        bootstrapDatepicker   : "../vendor/bootstrap-datepicker.min",
        bootstrapSelect       : "../vendor/bootstrap-select.min",
        dataTables            : "../../plugins/DataTables/DataTables-1.10.10/js/jquery.dataTables.min",
        monsterfy             : "monsterfy"
    }
});

// Carrega os arquivos principais e suas dependências.
requirejs([
    "jquery",
    "bootstrap",
    "bootbox",
    "bootstrapDatepicker",
    "bootstrapSelect",
    "dataTables",
    "monsterfy"
], function($, bootstrap, bootbox, bootstrapDatepicker, bootstrapSelect, dataTables, Monsterfy) {
    var jQuery = $;

    Monsterfy.initialize();
});
