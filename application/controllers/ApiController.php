<?php
#############################################################################
##   MonsterfyMVC - MVC Framework for PHP + MySQL                          ##
##   Copyright (C) 2012  Leandro Medeiros                                  ##
##                                                                         ##
##   This program is free software: you can redistribute it and/or modify  ##
##   it under the terms of the GNU General Public License as published by  ##
##   the Free Software Foundation, either version 3 of the License, or     ##
##   (at your option) any later version.                                   ##
##                                                                         ##
##   This program is distributed in the hope that it will be useful,       ##
##   but WITHOUT ANY WARRANTY; without even the implied warranty of        ##
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         ##
##   GNU General Public License for more details.                          ##
##                                                                         ##
##   You should have received a copy of the GNU General Public License     ##
##   along with this program.  If not, see <http://www.gnu.org/licenses/>. ##
##                                                                         ##
#############################################################################

/**
 * Controller da API
 *
 * @package controllers
 * @author  Leandro Medeiros
 * @since   2016-11-27
 * @link    http://bitbucket.org/leandro_medeiros/monsterfymvc
 */
class ApiController extends BaseController
{
    /**
     * Id do módulo correspondente à Controller
     * @var string
     */
    public static $moduleId = 5;

    /**
     * Define se o módulo pode ser acessado sem autenticação (override)
     * @var boolean
     */
    public static $backendController = false;

    /**
     * Exibir HomePage (override)
     *
     * @author Leandro Medeiros
     * @since  2016-11-27
     * @link   http:/bitbucket.org/leandro_medeiros/monsterfymvc
     *
     * @param  boolean  $forceRefresh   Forçar atualização dos Dados
     * @return mixed    Página
     */
    public function goHome($forceRefresh = false)
    {
        return $this->View->load();
    }

    public function listTasks()
    {
        $Tasks = new Task(new TaskDTO);
        return $this->View->load($Tasks->getList(true), true);
    }
}
