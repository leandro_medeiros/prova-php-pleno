<?php
#############################################################################
##   MonsterfyMVC - MVC Framework for PHP + MySQL                          ##
##   Copyright (C) 2012  Leandro Medeiros                                  ##
##                                                                         ##
##   This program is free software: you can redistribute it and/or modify  ##
##   it under the terms of the GNU General Public License as published by  ##
##   the Free Software Foundation, either version 3 of the License, or     ##
##   (at your option) any later version.                                   ##
##                                                                         ##
##   This program is distributed in the hope that it will be useful,       ##
##   but WITHOUT ANY WARRANTY; without even the implied warranty of        ##
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         ##
##   GNU General Public License for more details.                          ##
##                                                                         ##
##   You should have received a copy of the GNU General Public License     ##
##   along with this program.  If not, see <http://www.gnu.org/licenses/>. ##
##                                                                         ##
#############################################################################

/**
 * Classe base para uma DTO
 *
 * <p>Todas as DTO da aplicação devem estender esta.</p>
 *
 * @author Leandro Medeiros
 * @since  2015-07-08
 * @link   http://bitbucket.org/leandro_medeiros/monsterfymvc
 */
class BaseDTO
{
    /**
     * Chave primária
     * @var integer
     */
    public $id = 0;

    /**
     * Registro ativo ou não
     * @var boolean
     */
    public $active = true;

    public static function getClassForTable($tableName)
    {
        if (!preg_match_all('@([A-Za-z0-9]+)_*@', $tableName, $matches)) {
            $dtoName = $tableName;
        } else {
            $dtoName = '';
            array_shift($matches);

            foreach ($matches[0] as $entitie) {
                $dtoName .= ucfirst($entitie);
            }
        }

        $dtoName .= 'DTO';

        return $dtoName;
    }
}
