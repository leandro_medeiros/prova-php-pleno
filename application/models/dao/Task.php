<?php

/**
 * DAO do Módulo
 *
 * @package models
 * @author  Leandro Medeiros
 * @since   2016-11-27
 * @link    http://bitbucket.org/leandro_medeiros/monsterfymvc
 */
class Task extends BaseDAO
{
    /**
     * Construtor
     *
     * @author Leandro Medeiros
     * @since  2016-11-27
     * @link   http:/bitbucket.org/leandro_medeiros/monsterfymvc
     *
     * @param  TaskDTO $Dto
     */
    public function __construct(TaskDTO $Dto)
    {
        parent::__construct($Dto);
    }

    /**
     * Obter Lista (override)
     *
     * @author Leandro Medeiros
     * @since  2016-11-27
     * @link   http:/bitbucket.org/leandro_medeiros/monsterfymvc
     *
     * @param  boolean  $forDatatables  Preparar retorno para Datatables
     * @return array    Lista
     */
    public function getList($forDatatables = false)
    {
        $list = array();
        $sql = "
            SELECT t.user_id,
                   t.title,
                   t.description,
                   t.priority_id,
                   p.description AS priority,
                   u.name AS user
              FROM task t
              JOIN priority p ON t.priority_id = p.id
              JOIN user u ON t.user_id = u.id
             WHERE t.active
          ORDER BY t.priority_id
        ";

        if (!$this->Script->execute($sql)) {
            return $list;
        }

        if ($forDatatables) {
            $list['rows'] = array();

            foreach ($this->Script->dataset as $element) {
                $list['rows'][] = array(
                    $element['user_id'],
                    $element['title'],
                    $element['description'],
                    $element['priority'],
                    $element['user'],
                );
            }
        } else {
            foreach ($this->Script->dataset as $element) {
                $list[$element['id']] = Lib::datasetToDto(new TaskDTO, $element);
            }
        }

        return $list;
    }
}
