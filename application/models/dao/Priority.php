<?php

/**
 * DAO das Prioridades
 *
 * @package models
 * @author  Leandro Medeiros
 * @since   2016-11-27
 * @link    http://bitbucket.org/leandro_medeiros/monsterfymvc
 */
class Priority extends BaseDAO
{
    /**
     * Construtor
     *
     * @author Leandro Medeiros
     * @since  2016-11-27
     * @link   http:/bitbucket.org/leandro_medeiros/monsterfymvc
     *
     * @param  PriorityDTO $Dto
     */
    public function __construct(PriorityDTO $Dto)
    {
        parent::__construct($Dto);
    }

    public function __toString()
    {
        return $this->Dto->description;
    }
}
