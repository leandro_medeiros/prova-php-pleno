<?php

/**
 * DTO da Prioridade
 *
 * @package models
 * @author  Leandro Medeiros
 * @since   2016-11-27
 * @link    http://bitbucket.org/leandro_medeiros/monsterfymvc
 */
final class PriorityDTO extends BaseDTO
{
    /**
     * Descrição
     * @var string
     */
    public $description;
}
