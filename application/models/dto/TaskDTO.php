<?php

/**
 * DTO da Tarefa
 *
 * @package models
 * @author  Leandro Medeiros
 * @since   2016-11-27
 * @link    http://bitbucket.org/leandro_medeiros/monsterfymvc
 */
final class TaskDTO extends BaseDTO
{
    /**
     * ID do usuário que registrou a tarefa
     * @var integer
     */
    public $user_id;

    /**
     * Título
     * @var string
     */
    public $title;

    /**
     * Descrição
     * @var string
     */
    public $description;

    /**
     * ID da prioridade
     * @var integer
     */
    public $priority_id;

    /**
     * Prioridade
     * @var string
     */
    public $priority;

    /**
     * Usuário
     * @var string
     */
    public $user;
}
